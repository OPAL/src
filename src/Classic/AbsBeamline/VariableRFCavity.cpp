//
// Class VariableRFCavity
//   Defines the abstract interface for a RF Cavity
//   with Time Dependent Parameters.
//
// Copyright (c) 2014 - 2023, Chris Rogers, STFC Rutherford Appleton Laboratory, Didcot, UK
// All rights reserved
//
// This file is part of OPAL.
//
// OPAL is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// You should have received a copy of the GNU General Public License
// along with OPAL. If not, see <https://www.gnu.org/licenses/>.
//
#include "AbsBeamline/VariableRFCavity.h"

#include "AbsBeamline/BeamlineVisitor.h"
#include "Algorithms/PartBunchBase.h"
#include "Physics/Physics.h"
#include "Physics/Units.h"
#include "Utilities/GeneralClassicException.h"

#include <cmath>

VariableRFCavity::VariableRFCavity(const std::string& name): Component(name) {
    initNull();  // initialise everything to nullptr
}

VariableRFCavity::VariableRFCavity(): Component() {
    initNull();  // initialise everything to nullptr
}

VariableRFCavity::VariableRFCavity(const VariableRFCavity& var): Component() {
    initNull();  // initialise everything to nullptr
    *this = var;
}

VariableRFCavity& VariableRFCavity::operator=(const VariableRFCavity& rhs) {
    if (&rhs == this) {
        return *this;
    }
    setName(rhs.getName());
    setPhaseModel(nullptr);
    setAmplitudeModel(nullptr);
    setFrequencyModel(nullptr);
    if (rhs.phaseTD_m != nullptr) {
        setPhaseModel(std::shared_ptr<AbstractTimeDependence>(rhs.phaseTD_m->clone()));
    }
    if (rhs.amplitudeTD_m != nullptr) {
        setAmplitudeModel(std::shared_ptr<AbstractTimeDependence>(rhs.amplitudeTD_m->clone()));
    }
    if (rhs.frequencyTD_m != nullptr) {
        setFrequencyModel(std::shared_ptr<AbstractTimeDependence>(rhs.frequencyTD_m->clone()));
    }
    phaseName_m = rhs.phaseName_m;
    amplitudeName_m = rhs.amplitudeName_m;
    frequencyName_m = rhs.frequencyName_m;
    halfWidth_m = rhs.halfWidth_m;
    halfHeight_m = rhs.halfHeight_m;
    setLength(rhs.length_m);
    return *this;
}

VariableRFCavity::~VariableRFCavity() {
    // shared_ptr should self-destruct when they are ready
}

void VariableRFCavity::initNull() {
    length_m = 0.;
    phaseName_m = "";
    amplitudeName_m = "";
    frequencyName_m = "";
    halfHeight_m = 0.;
    halfWidth_m = 0;
    RefPartBunch_m = nullptr;
}

std::shared_ptr<AbstractTimeDependence> VariableRFCavity::getAmplitudeModel() const {
    return amplitudeTD_m;
}

std::shared_ptr<AbstractTimeDependence> VariableRFCavity::getPhaseModel() const {
    return phaseTD_m;
}

std::shared_ptr<AbstractTimeDependence> VariableRFCavity::getFrequencyModel() const {
    return frequencyTD_m;
}

void VariableRFCavity::setAmplitudeModel(std::shared_ptr<AbstractTimeDependence> amplitude_td) {
    amplitudeTD_m = amplitude_td;
}

void VariableRFCavity::setPhaseModel(std::shared_ptr<AbstractTimeDependence> phase_td) {
    phaseTD_m = phase_td;
}

void VariableRFCavity::setFrequencyModel(std::shared_ptr<AbstractTimeDependence> frequency_td) {
    frequencyTD_m = frequency_td;
}

StraightGeometry& VariableRFCavity::getGeometry() {
    return geometry;
}

const StraightGeometry& VariableRFCavity::getGeometry() const {
    return geometry;
}

EMField& VariableRFCavity::getField() {
  throw GeneralClassicException("VariableRFCavity",
                      "No field defined for VariableRFCavity");
}

const EMField& VariableRFCavity::getField() const {
  throw GeneralClassicException("VariableRFCavity::getField",
                                "No field defined for VariableRFCavity");
}


bool VariableRFCavity::apply(const size_t& i, const double& t,
                             Vector_t& E, Vector_t& B) {
    return apply(RefPartBunch_m->R[i], RefPartBunch_m->P[i], t, E, B);
}

// If this is too slow: a quicker implementation would be to use templates not
// inheritance (vtable lookup is removed). This is in the inner
// tracking loop, so low level optimisation is possibly worthwhile.
//
// Do I need bound checking here? I have no "radius" parameter, but I do have a
// "length".
bool VariableRFCavity::apply(const Vector_t& R, const Vector_t& /*P*/,
                             const double& t, Vector_t& E, Vector_t& /*B*/) {
    if (R[2] >= 0. && R[2] < length_m) {
        if (std::abs(R[0]) > halfWidth_m || std::abs(R[1]) > halfHeight_m) {
            return true;
        }
        double E0 = amplitudeTD_m->getValue(t);
        double f = frequencyTD_m->getValue(t) * Units::MHz2Hz * Units::Hz2GHz; // need GHz on the element we have MHz
        double phi = phaseTD_m->getValue(t);
        E = Vector_t(0., 0., E0 * std::sin(Physics::two_pi * f * t + phi));
        return false;
    }
    return true;
}

bool VariableRFCavity::applyToReferenceParticle(const Vector_t& R, const Vector_t& P,
                                                const double& t, Vector_t& E, Vector_t& B) {
    return apply(R, P, t, E, B);
}

void VariableRFCavity::initialise(PartBunchBase<double, 3>* bunch, double& /*startField*/, double& /*endField*/) {
    RefPartBunch_m = bunch;
}

void VariableRFCavity::finalise() {
    RefPartBunch_m = nullptr;
}

ElementBase* VariableRFCavity::clone() const {
    return new VariableRFCavity(*this);
}

void VariableRFCavity::accept(BeamlineVisitor& visitor) const {
    initialise();
    visitor.visitVariableRFCavity(*this);
}

void VariableRFCavity::initialise() const {
    VariableRFCavity* cavity = const_cast<VariableRFCavity*>(this);
    std::shared_ptr<AbstractTimeDependence> phaseTD =
        AbstractTimeDependence::getTimeDependence(phaseName_m);
    cavity->setPhaseModel(std::shared_ptr<AbstractTimeDependence>(phaseTD->clone()));
    std::shared_ptr<AbstractTimeDependence> frequencyTD =
        AbstractTimeDependence::getTimeDependence(frequencyName_m);
    cavity->setFrequencyModel(std::shared_ptr<AbstractTimeDependence>(frequencyTD->clone()));
    std::shared_ptr<AbstractTimeDependence> amplitudeTD =
        AbstractTimeDependence::getTimeDependence(amplitudeName_m);
    cavity->setAmplitudeModel(std::shared_ptr<AbstractTimeDependence>(amplitudeTD->clone()));

    if (halfHeight_m < 1e-9 || halfWidth_m < 1e-9)
        throw GeneralClassicException("VariableRFCavity::initialise",
                                      "Height or width was not set on VariableRFCavity");
}

void VariableRFCavity::setLength(double length) {
    length_m = length;
    geometry.setElementLength(length_m);
}
