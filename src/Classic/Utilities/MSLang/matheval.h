#ifndef MSLANG_MATHEVAL_H
#define MSLANG_MATHEVAL_H

#include <string>

namespace mslang {
    double parseMathExpression(const std::string &str);
}
#endif