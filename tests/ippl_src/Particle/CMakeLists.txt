set (_SRCS
    EdgeCentering.cpp
    ParticleDebug.cpp
)

include_directories (
  ${CMAKE_CURRENT_SOURCE_DIR}
)

add_sources(${_SRCS})
